import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class quoteDataCSV {

    public static FileWriter quoteWriter = null;
    public static FileWriter tradeWriter = null;

    public String quoteFile = "quoteData1.csv";
    public String tradeFile = "tradeData1.csv";

    public int numTrades = 10;
    public int genDays = 5;

    private static List instru = new ArrayList();



    private static void generateQuotes(String Instrument, int NumDays) throws IOException {
        String name = Instrument;
        Date date;
        double closePrice;
        final Calendar cal = Calendar.getInstance();
        ArrayList quote = new ArrayList();
        for(int i=0;i<NumDays;i++){
            //new date -i
            cal.add(Calendar.DATE, -1);
            if(cal.get(Calendar.DAY_OF_WEEK)==Calendar.SUNDAY){
                cal.add(Calendar.DATE, -2);
            }else if(cal.get(Calendar.DAY_OF_WEEK)==Calendar.SATURDAY){
                cal.add(Calendar.DATE, -1);
            }
            date = cal.getTime();

            //new close price
            Random r = new Random();
            double randVal =r.nextDouble()*(50-40)+40;
            closePrice = randVal*40;

            //write to csv name, date, closePrice
            String [] x = {name, String.valueOf(date), String.valueOf(closePrice)};
            quote.add(0,(x[0]+", "+x[1]+", "+x[2]));
        }

        try {

            for(int i =0; i<quote.size();i++){
                quoteWriter.append(String.valueOf(quote.get(i)));
                quoteWriter.append("\n");

            }
        }catch (Exception e){
            System.out.println("ERROR");
            e.printStackTrace();
        }
    }

    private static void generateTradeData(ArrayList quote, ArrayList prevQuote, int numTrades, int count){

        String prevName = (String) prevQuote.get(0);
        String prevPrice = (String) prevQuote.get(2);

        String name = (String) quote.get(0);
        String date = (String) quote.get(1);
        double closePrice = Double.valueOf(quote.get(2).toString());
        double openPrice;
        double price;



        //set open price to previous days close price or generate one if doesn't exist
        if(name.equals(prevName)){
            openPrice = Double.valueOf(prevPrice);
        }
        else{
            Random r = new Random();
            double max = closePrice+534.248621458621;
            double min = closePrice-534.248621458621;
            openPrice = min + (max-min)*r.nextDouble();
        }
        double max = Math.max(openPrice,closePrice);
        double min = Math.min(openPrice,closePrice);

        //generate data trades n times and end on current days close price(found in quotes csv)
        try {
            tradeWriter.append("\nName, Date, Open price, Close price\n");
            tradeWriter.append(name+","+date+","+openPrice+","+closePrice+"\n\n");
            tradeWriter.append("Date, Time, Price, Volume\n");

        } catch (IOException e) {
            e.printStackTrace();
        }
//        System.out.println("\n"+name+","+date+","+openPrice+","+closePrice);

        ArrayList trades = new ArrayList();

        for(int j=0;j<numTrades;j++){
            Date today = new Date();
            DateFormat dateFormat = new SimpleDateFormat("EEEEE dd MMMMM yyyy"); // format pattern to display the date
            date = dateFormat.format(today);// supply today's date
            Time time = new Time(28800000 + (21000 * (j*count)));//09:00:00 + 21secs for each new trade - smallerNumber=smaller time gaps
            Random r = new Random();
            if(j<(numTrades/2)){
                price = Math.min((min + (max - min) * r.nextDouble()), (min + (max - min) * r.nextDouble()));

            }
            else {
                price = Math.max((min + (max - min) * r.nextDouble()), (min + (max - min) * r.nextDouble()));
            }
            int vol = 100 + r.nextInt(1000-100+1);

            if(j==numTrades-1){price = closePrice;}

//            System.out.println("Date: "+date+" Time: "+time+" Price: "+price+" Volume: "+vol);
            trades.add((date+","+time+","+price+","+vol));
        }

        try {

            for(int i =0; i<trades.size();i++){

                tradeWriter.append(String.valueOf(trades.get(i)));
                tradeWriter.append("\n");
            }
            System.out.println("SUCCESS");
        }catch (Exception e){
            System.out.println("ERROR");
            e.printStackTrace();
        }
    }



    public void genQuoteData () throws IOException {

        String header = "Instrument, Date, Close Price\n";
        quoteWriter = new FileWriter(quoteFile);
        quoteWriter.append(header);

        for(int i=0;i<instru.size();i++){
            generateQuotes(instru.get(i).toString(),genDays);
        }

        try{
            quoteWriter.flush();
            quoteWriter.close();
        }catch (IOException e){
            System.out.println("BOOM");
        }
    }

    public void genTradeData () throws IOException {


        tradeWriter = new FileWriter(tradeFile);

        ArrayList allQuotes = new ArrayList();


        try (BufferedReader br = new BufferedReader(new FileReader(quoteFile))) {
            String line;

            while ((line = br.readLine()) != null) {
                String[] info = line.split(",");
                ArrayList quoteData = new ArrayList();
                for (String data : info) {
                    quoteData.add(data);
                }
                allQuotes.add(quoteData);
            }

            for (int i = 1; i < allQuotes.size(); i++) {

                ArrayList x = (ArrayList) allQuotes.get(i);
                ArrayList y = (ArrayList) allQuotes.get(i - 1);

                generateTradeData(x, y, numTrades, i);


            }

            try {
                tradeWriter.flush();
                tradeWriter.close();
            } catch (IOException e) {
                System.out.println("BOOM");
            }


        }
    }



    public static void main(String[] args) throws IOException {

        quoteDataCSV sim = new quoteDataCSV();


        instru.add("Vodafone");
        instru.add("HSBC");


        sim.genQuoteData();
        sim.genTradeData();


        //replay tool that plays the data back and calls newTrade, newQuote and stores the data

        //implement hash-maps for each instrument that stores the highs and lows of each instrument on each day
        //include validation


    }







}
